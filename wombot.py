import sys
import dataset
import requests
from time import sleep
from urllib.parse import urlencode, urljoin
from bs4 import BeautifulSoup as bs
from mastodon import Mastodon
from config import api_client


api = Mastodon(**api_client)
db = dataset.connect('sqlite:///content_farm.db')
img_table = db['images']
toot_table = db['toots']


def shorten_url(url):
    r = requests.post('https://ptpb.pw/u', data={'c': url})
    return [line for line in r.text.split('\n') if line.startswith('url: ')][0].split()[1]


def get_images():
    _base_url = 'https://ccsearch.creativecommons.org/'
    _params = {'search': 'wombat', 'search_fields': 'tags', 'per_page': 100,
               'work_types': 'photos', 'providers': 'flickr'}
    figs = []
    ims = []
    for page in range(3):
        _params['page'] = page+1
        r = requests.get('?'.join([_base_url, urlencode(_params)]))
        if r.ok:
            print(page)
            s = bs(r.text, 'lxml')
            figs.extend(s.findAll('figure'))
            print(len(figs))
        else:
            break
    for fig in figs:
        d = {}
        d['detail_link'] = urljoin(_base_url, fig.find('a', {'class': 't-detail-link'})['href'])
        d['img_url'] = fig.find('img')['src']
        d['title'] = fig.find('p', {'class': 'title'}).text
        ims.append(d)
    return ims


def update_db():
    images = get_images()
    img_table.insert_many(images)


def toot():
    q = db.query('SELECT * FROM images WHERE img_url NOT IN (SELECT img_url FROM toots) ORDER BY random() LIMIT 1')
    for i in q:
        print('Found an image to toot! Review now. You have 20 seconds.\n>>>', i['img_url'])
        sleep(20)
        r = requests.get(i['img_url'])
        r.raise_for_status()
        with open('temp.jpeg', mode='w+b') as f:
            f.write(r.content)
            media = api.media_post(f.name)
        short_link = shorten_url(i['detail_link'])
        print(api.status_post('#wombats ({})'.format(short_link),
                              media_ids=[media],
                              sensitive=False,
                              visibility='public'))
        toot_table.insert(dict(img_url=i['img_url']))


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print('What is your bidding?')
    if sys.argv[1] == 'toot':
        toot()
    if sys.argv[1] == 'harvest':
        update_db()
